
# Obstacle Assault

●	Small Unreal Engine 5 platformer demo done with C++ and Blueprint.

●	Demo developed to understand the following concepts UPROPERTY variables, game modes, blueprint child classes Tick function, delta time, vectors, transformations and rotations in applied context of game development.

● This demo was developed as part of the Unreal Engine 5 Udemy Course I took.








# Screenshots

![App Screenshot](https://gitlab.com/pedroalmd/obstacleassault/-/raw/main/Screenshots/Screenshot%202023-10-07%20174832.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/obstacleassault/-/raw/main/Screenshots/Screenshot%202023-10-07%20174636%20-%20Copy.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/obstacleassault/-/raw/main/Screenshots/Screenshot%202023-10-07%20174731.png?ref_type=heads)
![App Screenshot](https://gitlab.com/pedroalmd/obstacleassault/-/raw/main/Screenshots/Screenshot%202023-10-07%20174811.png?ref_type=heads)



## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/pedroalmd/obstacleassault
```

Go to the project directory

```bash
 ObstacleAssault/obstacleassault
```

Run executable and enjoy the demo!

```bash
  ObstacleAssault.exe
```





## Objective

- Navigate through the platforms to reach the house at the top of the staircase!

